Bite-sized - a collection of small exercises. 
====

This is just a collection of quick and dirty prototypes of various things. 


This repo is branched in accordance with the type of platform that was used for the work. 



Cinder Notes
====
* For Cinder, you should be able to get those projects up and running by following these guidelines.
    * All things are placed in your OS's "Documents" folder 
    * Cinder is cloned into a folder called "libraries"
    * [Kanpai](https://gitlab.com/sortofsleepy/kanpai) is cloned into Cinder's "blocks" folder
    * `bite-sized` is cloned into your Documents folder. 

If it is all set up in that manner, things "should" work though keep in mind that some projects may or may not be finished. Also note that this set up has only been verified on Windows. Also note there may be some legacy project setups for one or 2 projects in there that would require you to adjust the paths to Cinder.

